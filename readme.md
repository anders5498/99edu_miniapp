# 久久教育 - 微信小程序

------

整个项目一共7个页面，富文本显示使用了[wxParse](https://github.com/icindy/wxParse)组件，主要实现了首页，课程分类页，课程详情页，课时页，搜索页，观看记录等页面，达到了免费视频教程小程序的基本需求。

## API文档

------

> https://www.showdoc.cc/99edu

## 预览地址

------

项目涉及到视频，个人开发者无法通过审核，小程序没有上线，如果需要测试请联系我，加入到体验成员中方可使用。

![未过审](http://188.131.159.220/%E6%9C%AA%E8%BF%87%E5%AE%A1.png)

小程序二维码：![mimiapp](http://188.131.159.220/miniapp.jpg)

内测二维码： ![miniapp_test](http://188.131.159.220/miniapp_test.jpg)

## 实现功能

------

- [x] 热门推荐课程（轮播图banner）
- [x] 最新课时
- [x] 课时搜索
- [x] 课程分类
- [x] 课程详情（介绍，课时列表）
- [x] 课时详情（播放页，讲师介绍）
- [x] 播放历史记录（需要微信登录授权）

## TODO

- [ ] 首页常用按钮由接口控制
- [ ] 教程视频购买
- [ ] 讲师详情页（TA的课程）

## 屏幕截图

------

![home](http://188.131.159.220/mini_home.jpg)

![search](http://188.131.159.220/mini_search.jpg)

![class](http://188.131.159.220/mini_class.jpg)

![course](http://188.131.159.220/mini_course.jpg)

![category](http://188.131.159.220/mini_category.jpg)

![history](http://188.131.159.220/mini_history.jpg)

![login](http://188.131.159.220/mini_login.jpg)

## 问题

------

记录在项目中遇到的一些问题，和解决方法

-  Tabs（标签页）切换
   -  MVVM思想，数据驱动视图，在data中添加一个数组保存tabs状态，给每个tab绑定一个点击事件，点击时更新data的数据进而改变tab的样式
  -  向上滑动到底，加载更多
       -  一开始想着用scroll-view，后来发现有onReachBottom函数，页面上拉触底事件的处理函数。
       -  但是，向下滑动会弹起来，页面不动的时候才触发函数，加载数据，感觉有点难受，体验不好
       -  **想做成支付宝那种，用力上拉才会加载更多。**
  -  下拉刷新
       -  onPullDownRefresh函数，监听用户下拉动作，配合wx.showNavigationBarLoading();显示顶部刷新图标，用户体验更好。
