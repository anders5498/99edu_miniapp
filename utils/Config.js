// 接口域名 文档地址：https://www.showdoc.cc/99edu
const domain = 'https://yp.jtcode.xyz/api/v1';

export const Config = {
  // banner列表
  banner_list: `${domain}/banners`,

  // 最新课时列表 可以下拉获取
  class_list: `${domain}/classes`,
  // 课时详情
  class_one: `${domain}/classes/`,

  // 分类检索
  cate_list: `${domain}/categories`,

  // 课程列表
  course_list: `${domain}/courses`,
  // 课程详情
  course: `${domain}/courses/`,

  // code换取openID
  code2id: `${domain}/code2Session`,
  // 提交用户信息 post
  userinfo: `${domain}/userinfo`,
  // 录入观看记录                  id
  recording: `${domain}/recording/`,
  // 获取观看记录
  history: `${domain}/history/`,
  // 播放次数+1            id
  count: `${domain}/count/`
}