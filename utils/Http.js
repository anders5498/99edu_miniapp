// http请求类
class Http {
    /**
     * 发起请求 返回的是一个promise对象
     * @param {object} param0  url,data={},method='GET'
     */
    httpRequest({ url, data = {}, method = 'GET' }) {
        return new Promise((res, rej) => {
            wx.request({
                url,
                data,
                method,
                success: res,
                fail: rej
            });
        })
    }
}
// 导出
export default Http;