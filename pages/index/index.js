import IndexModel from './model';
let model = new IndexModel();
//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    stime: 0,
    etime: 0,
    color: "#000",
    banner: [],
    // 最新课程 添加时间倒序
    classes: []
  },
  //事件处理函数
  bindViewTap: function () {

  },
  onLoad: function () {
    // 加载banner
    model.getBannerList(this).then(res=>{
      this.setData({
        banner:res.data
      })
    })
    model.getNewClassList(this).then(res => {
      this.setData({
        classes:res.data.data
      })
    })
    
  },
  gotoSearch() {
    wx.navigateTo({
      url: '/pages/search/search'
    });
  }
})