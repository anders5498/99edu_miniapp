// 配置文件
import {Config} from "../../utils/Config";
// 请求类
import Http from "../../utils/Http";
/**
 * 首页模型
 */
class IndexModel extends Http{
    // 获取banner列表
    getBannerList(obj) {
        var url = Config.banner_list;
        return this.httpRequest({url});
    }

    // 获取最新的课程列表五个
    getNewClassList(obj) {
        var url = Config.class_list+'?limit=5';
        return this.httpRequest({url});
    }
}

export default IndexModel;