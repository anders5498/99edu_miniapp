import { Config } from '../../utils/Config'
import Http from '../../utils/Http'
class MineModel extends Http {
    // 记录code
    code2Session() {
        return new Promise((res, rej) => {
            wx.login({
                timeout: 10000,
                success: res,
                fail: rej
            });
        }).then(res => {
            let data = { js_code: res.code };
            let url = Config.code2id
            return this.httpRequest({ url, data }).then(res => {
                if (res.data.errcode == 0) {
                    wx.setStorageSync('loginSessionKey', res.data.loginSessionKey);
                } else {
                    console.log(res.data.errmsg)
                }
            })
        }, rej => {
            console.log('登录时网络错误');
        })
    }

    // 记录用户信息
    postUserinfo({ obj, data }) {
        let url = Config.userinfo;
        return this.httpRequest({ url, data, method: 'POST' }).then(res => {
            if (res.data.errcode == 0) {
                wx.setStorage({
                    key: 'userinfo',
                    data,
                    success: res => {
                        obj.setData({
                            userInfo: data,
                            isLogin: true
                        })
                    }
                });
            }
        }, rej => {
            console.log('授权时网络错误')
        })
    }

    // 获取观看记录
    getHistory({ obj, data = {}, refresh = false }) {
        // loading
        wx.showLoading({
            title: '玩命加载中',
            mask: true
        });

        let id = wx.getStorageSync('loginSessionKey');
        let url = Config.history + id;
        return this.httpRequest({ url, data }).then(res => {
            // 隐藏loading
            wx.hideLoading();
            let history = res.data.data;
            // 格式化时间

            history.forEach((v, i) => {
                history[i].datetime = this.formattime(history[i].datetime)
            });

            if (refresh) {
                // 刷新
                obj.setData({
                    history,
                    offset: data.offset,
                    limit: data.limit
                })
            } else {
                if (history.length > 0) {
                    // 下拉更多
                    obj.setData({
                        history: obj.data.history.concat(history),
                        offset: data.offset,
                        limit: data.limit
                    })
                }else{
                    wx.showToast({
                        title: '没有更多了',
                        icon: 'none',
                        duration: 1500,
                        mask: false
                    });
                }
            }
        })
    }


    // 当前时间戳
    formattime(time) {
        let timestamp = Date.parse(new Date()) / 1000;
        let cha = timestamp - time;
        if (cha < 60) {
            return cha + '秒之前';
        } else if (cha < 3600) {
            return Math.floor(cha / 60) + '分钟之前';
        } else if (cha < 86400) {
            return Math.floor(cha / 3600) + '小时之前';
        } else {
            return Math.floor(cha / 86400) + '天之前';
        }
        console.log(cha);
        console.log(timestamp);
    }
}

export default MineModel;