// 配置文件
import { Config } from "../../utils/Config";
// 请求类
import Http from "../../utils/Http";
class Classlist extends Http {
    /**
     * 获取课时列表
     * @param {object} param0 {page对象, 数据(3个参数), 是否是刷新页面}
     */
    getClassList({ obj, data = {}, refresh = false }) {
        // loading
        wx.showLoading({
            title: '玩命加载中',
            mask: true
        });

        var url = Config.class_list;

        return this.httpRequest({ url, data }).then(res => {
            // 隐藏loading
            wx.hideLoading();
            // 接口数据
            let newData = res.data.data;

            // 下拉刷新页面
            if (refresh) {
                obj.setData({
                    classes: newData,
                    offset: data.offset,
                    limit: data.limit,
                    kw: data.kw
                })
                 return;
            }

            // 上拉获取更多
            if (newData.length > 0) {
                let classes = obj.data.classes.concat(newData)
                obj.setData({
                    classes,
                    offset: data.offset,
                    limit: data.limit,
                    kw: data.kw
                })
            } else {
                wx.showToast({
                    title: '没有更多了',
                    icon: 'none',
                    duration: 1500
                });
            }
        });
    }
}
export default Classlist;