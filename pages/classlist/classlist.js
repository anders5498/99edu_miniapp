// pages/classlist/classlist.js
import ClasslistModel from './model';
let model = new ClasslistModel();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    height: 0,
    offset: 0,
    limit: 5,
    kw: '',
    classes: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 滚动高度自适应
    wx.getSystemInfo({
      success: (result) => {
        this.setData({
          height: result.windowHeight
        })
      }
    });
    // 调用model获取课程列表
    let data = {
      offset: 0,
      limit: 5,
      kw: ''
    }
    model.getClassList({
      obj: this,
      data
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   * 刷新页面
   */
  onPullDownRefresh() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();

    let data = {
      offset: 0,
      limit: 5,
      kw: '',
    };
    // 调用model获取课程列表
    model.getClassList({
      obj: this,
      data,
      refresh: true
    }).then(res => {
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let data = {
      offset: this.data.offset + 5,
      limit: this.data.limit,
      kw: this.data.kw
    };
    model.getClassList({ obj: this, data });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  lower(e) {
    console.log(e)
  },

  more(e) {
    console.log(e)
  },
  // 搜索
  sou(e) {
    // 赋值kw
    let kw = e.detail.value;
    let data = {
      offset: 0,
      limit: 5,
      kw
    };
    // 调用接口
    model.getClassList({ obj:this, data, refresh:true });
  }
})