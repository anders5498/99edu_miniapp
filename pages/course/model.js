import {
  Config
} from '../../utils/Config'
import Http from '../../utils/Http'

class CourseModel extends Http {
  getCourse({
    obj,
    course_id
  }) {
    let url = Config.course + course_id;
    return this.httpRequest({
      url
    }).then(res => {
      obj.setData({
        course: res.data.data
      })
    })
  }
}
export default CourseModel;