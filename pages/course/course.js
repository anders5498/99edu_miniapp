// pages/course/course.js
import CourseModel from './model'
let model = new CourseModel()
var WxParse = require('../../wxParse/wxParse.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    course:{},
    bar: ['selected','','']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let course_id = options.course_id;
    model.getCourse({
      obj:this,
      course_id
    }).then(res => {
      let course = this.data.course;
      WxParse.wxParse('intro', 'html', course.intro, this, 0);
      this.setData({
        course
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 切换
  toggle(e){
    let id = e.currentTarget.dataset.id;
    let bar = ['', '',''];
    bar[id] = 'selected';
    this.setData({
      bar
    })
  }
})