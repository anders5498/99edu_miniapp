// pages/class/class.js
import ClassModel from './model';
let model = new ClassModel();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    class_info: {},
    bar: ['selected', ''],
    id: 0,//课时id
    issave: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let id = options.class_id;
    this.setData({
      id
    })
    model.getClass(id).then(res => {
      let class_info = res.data.data;
      this.setData({
        class_info
      })
    })
  },

  // 点击切换
  toggle(e) {
    let id = e.currentTarget.dataset.id;
    let bar = ['', ''];
    bar[id] = 'selected';
    this.setData({
      bar
    })
  },

  // 点击观看，播放次数加一，保存到播放记录
  saveinfo() {
    let issave = this.data.issave;
    // 播放次数加一
    model.countInc({ obj: this })
    // 保存到播放记录
    if (!issave) {
      let id = wx.getStorageSync('loginSessionKey');
      if (!id) return false;
      let data = {
        classid: this.data.id
      };
      model.recording({
        obj: this,
        id,
        data
      })
    }
  }
})