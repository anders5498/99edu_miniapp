import { Config } from '../../utils/Config';
import Http from '../../utils/Http';
class Class extends Http {
    // 获取课时详情
    getClass(id) {
        let url = Config.class_one + id;
        return this.httpRequest({ url });
    }

    // 录入观看记录
    recording({ obj, id, data }) {
        let url = Config.recording + id;
        return this.httpRequest({ url, data, method: 'POST' }).then(res => {
            if (res.data.status === 0) {
                // 成功之后保存到本地缓存，就可以保证数据一致了

                // 修改状态
                obj.setData({
                    issave: true
                })
            }
        })
    }

    // 播放次数+1
    countInc({ obj }) {
        let url = Config.count + obj.data.id;
        return this.httpRequest({ url, data: {}, method: 'POST' }).then(res => {
            if (res.data.status === 0) {
                console.log('播放次数+1')
            } else {
                console.log('播放次数自增失败')
            }
        })
    }
}
export default Class;