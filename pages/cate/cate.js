// pages/cate/cate.js

import CateModel from './model';
let model = new CateModel();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 高度
    height: 0,
    // 分类列表数据
    cate_list: [],
    // 被选中分类 数组
    cate_selected: [],
    // 分类下课程列表
    course_list: [],
    // 偏移量
    offset: 0,
    // 条数
    limit: 5,
    cate_index: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 高度自适应
    wx.getSystemInfo({
      success: (result) => {
        this.setData({
          height: result.windowHeight - 10
        })
      }
    });

    // 获取分类数据
    model
      .getCatelist({ obj: this })
      .then(res => {
        let data = {
          category_id: this.data.cate_list[0].id,
          offset: 0,
          limit: 5
        }
        model.getCoursesByCate({
          obj: this,
          data
        });
      });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  // 切换分类
  changeCate(e) {
    let cate_index = e.currentTarget.dataset.id
    // 改变cate_selected数组的第cate_id个
    // 重置数组
    let cate_selected = Array.from({ length: this.data.cate_selected.length }, (v, k) => {
      if (k === cate_index) return 'selected';
      return '';
    });
    this.setData({
      cate_selected,
      cate_index
    })

    // 组装查询数据
    let data = {
      category_id: this.data.cate_list[cate_index].id,
      offset: 0,
      limit: 5
    };
    // 获取分类下课程
    model.getCoursesByCate({
      obj: this,
      data
    })
  },

  // 获取更多
  getMore() {
    // 当前分类id
    let cate_index = this.data.cate_index;
    // 组装查询数据
    let data = {
      category_id: this.data.cate_list[cate_index].id,
      offset: this.data.offset + this.data.limit,
      limit: this.data.limit
    };
    // 获取分类下课程
    model.getCoursesByCate({
      obj: this,
      data,
      additional:true
    })
  }

})