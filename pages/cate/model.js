import { Config } from '../../utils/Config';
import Http from '../../utils/Http';
class CateModel extends Http {
    // 获取分类列表
    getCatelist({ obj }) {
        // 请求接口
        let url = Config.cate_list;

        return this.httpRequest({ url }).then(res => {

            let cate_list = res.data;
            // 生成分类选中数组
            let cate_selected = Array.from({ length: cate_list.length }, (v, k) => {
                if (k === 0) return 'selected';
                return '';
            });
            // 赋值
            obj.setData({
                cate_list,
                cate_selected
            })
        });
    }

    // 获取分类下的课程列表
    getCoursesByCate({ obj, data = {}, additional = false }) {
        // loading
        wx.showLoading({
            title: '玩命加载中',
            mask: true
        });
        let url = Config.course_list;
        return this.httpRequest({ url, data }).then(res => {
            // 隐藏loading
            wx.hideLoading();
            // api数据
            let course_list = res.data.data;

            if (additional) {
                if(!course_list.length) {
                    // 没更多数据了
                    wx.showToast({
                        title: '没有更多了',
                        icon: 'none',
                        duration: 1500
                    });
                    return;
                }
                // 追加
                obj.setData({
                    course_list: obj.data.course_list.concat(course_list),
                    offset: data.offset,
                    limit: data.limit
                })
            } else {
                // 覆盖
                obj.setData({
                    course_list: course_list,
                    offset: data.offset,
                    limit: data.limit
                })
            }
        });
    }
}

export default CateModel;