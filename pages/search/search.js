// pages/search/search.js
import SearchModel from './model';
let model = new SearchModel();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    kw:'',
    offset:0,
    limit:5,
    course_list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let data = {
      offset: this.data.offset + this.data.limit,
      limit: this.data.limit,
      kw: this.data.kw
    }
    model.getCourse({
      obj:this,
      data,
      search:true
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //  搜索
  sou(e){
    // 搜索关键字
    let kw = e.detail.value;
    let data = {
      offset: 0,
      limit: 5,
      kw
    }
    model.getCourse({
      obj:this,
      data,
      search:true
    })
  }
})