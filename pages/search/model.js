import {Config} from '../../utils/Config';
import Http from '../../utils/Http';

class SearchModel extends Http {
    getCourse({obj,data={},search=false}){
        // loading
        wx.showLoading({
            title: '玩命加载中',
            mask: true
        });
        let url = Config.course_list;
        return this.httpRequest({url,data}).then(res => {// 隐藏loading
            wx.hideLoading();
            let course_list = res.data.data;
            // 上拉获取更多
            if (course_list.length > 0) {

                if(!search){
                    course_list = obj.data.course_list.concat(course_list)
                }

                obj.setData({
                    course_list,
                    offset: data.offset,
                    limit: data.limit,
                    kw: data.kw
                })
            } else {
                wx.showToast({
                    title: '没有更多了',
                    icon: 'none',
                    duration: 1500
                });
            }
        })
    }
}
export default SearchModel;